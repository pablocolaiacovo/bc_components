const _ = require('lodash')

module.exports = {
  prompt: ({ inquirer, args }) => {
    const questions = []
    if (typeof args.name === 'undefined') {
      questions.push({
        type: 'input',
        name: 'name',
        message: 'Name:',
        validate (value) {
          if (!value.length) {
            return 'Components must have a name.'
          }
          const fileName = _.kebabCase(value)
          if (fileName.indexOf('-') === -1) {
            return 'Component names should contain at least two words to avoid conflicts with HTML elements.'
          }
          return true
        }
      })
    }
    questions.push(
      {
        type: 'select',
        name: 'type',
        choices: [
          {
            name: 'sfc',
            message: 'Single Vue File'
          },
          {
            name: 'mfc',
            message: 'Separate Files (.html, .scss, .js, .vue)'
          }
        ]
      }
    )

    return inquirer.prompt(questions).then(answers => {
      const { type } = answers
      const name = answers.name || args.name
      const questions = []

      if (type === 'mfc') {
        questions.push({
          type: 'multiselect',
          name: 'files',
          message: 'Select component files to use:',
          initial: ['script', 'template', 'style'],
          choices: [
            {
              name: 'script',
              message: `${name}.js`
            },
            {
              name: 'template',
              message: `${name}.html`
            },
            {
              name: 'style',
              message: `${name}.scss`
            }
          ],
          validate (value) {
            if (value.indexOf('script') === -1 && value.indexOf('template') === -1) {
              return 'Components require at least a script or a template.'
            }
            return true
          }
        })
      }
      questions.push({
        type: 'multiselect',
        name: 'extras',
        message: 'Component Extras',
        choices: [
          {
            name: 'test',
            message: `${name}.spec.js`
          },
          {
            name: 'story',
            message: `${name}.stories.js`
          }
        ]
      })

      return inquirer
        .prompt(questions)
        .then(nextAnswers => Object.assign({}, answers, nextAnswers))
    })
  }
}
