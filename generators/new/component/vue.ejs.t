---
to: 'src/components/<%= name %>/index.vue'
---
<%
if (typeof files != 'undefined') {
if (files.indexOf('template') !== -1) {
%><template src="./<%= name %>.html" />
<%
}
if (files.indexOf('script') !== -1) {
%><script src="./<%= name %>.js"></script>
<%
}
if (files.indexOf('style') !== -1) {
%><style src="./<%= name %>.scss" scoped lang="scss"></style>
<%
}
}
else {
%><template>
  <div>
    Hello
  </div>
</template>

<script>
export default {
  name: '<%= name %>',
  data () {
    return {}
  }
}
</script>

<style scoped lang="scss">
</style>
<%
}
%>