---
to: "<%= extras.indexOf('story') !== -1 ? `src/components/${name}/${name}.stories.js` : null %>"
---
import <%= name %> from './'

export default {
  title: '<%= name %>'
}

export const Normal = () => ({
  components: { <%= name %> },
  template: '<<%= name %> />',
  data: () => ({})
})

Normal.story = {
  name: 'normal'
}