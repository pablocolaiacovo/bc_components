import IngredientRow from './components/IngredientRow'
import IngredientTable from './components/IngredientTable'
import IngredientForm from './components/IngredientForm'

const BCComponents = {
  IngredientRow,
  IngredientTable,
  IngredientForm,
}

BCComponents.install = function (Vue) {
  Object.keys(BCComponents).forEach((key) => {
    const Component = BCComponents[key]
    Vue.component(Component.name, Component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(BCComponents, window.BCComponentsConfig)
}

export default BCComponents

export { IngredientRow, IngredientTable, IngredientForm }
