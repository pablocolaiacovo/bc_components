# Baking Calculator Components
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

This component library consists of all the components used within the Baking Calculator Project.

## Packages

- [@aaroneous/ingredient-row](src/components/IngredientRow)
- [@aaroneous/ingredient-table](src/components/IngredientTable)
- [@aaroneous/ingredient-form](src/components/IngredientForm)

## Scripts

| Command                   | Description                                                  |
|:--------------------------|:-------------------------------------------------------------|
| `yarn build`              | Builds the entire library into a *dist* folder.              |
| `yarn test:unit`          | Runs unit tests                                              |
| `yarn lint`               | Runs the linter without fixing                               |
| `yarn format`             | Formats all the things                                       |
| `yarn storybook`          | Starts Storybook on `localhost:6006`                         |
| `yarn build-storybook`    | Builds the Storybook                                         |
| `yarn generate component` | Generates a boilerplate set of files for a single component. |
| `lerna run build`         | Bundles each component into it's own *dist* folder.          |
