module.exports = {
  setupFilesAfterEnv: ['<rootDir>/tests/unit/matchers'],
  testMatch: ['**/(*.)spec.js'],
  preset: '@vue/cli-plugin-unit-jest',
}
